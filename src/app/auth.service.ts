import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLogged = false;

  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) { }

  login(mail: string, pass: string): Observable<any> {
    return this.http.post('http://localhost:3000/login', {
      email: mail,
      password: pass
    }, {responseType: 'text'})
    .pipe(
      map((user) => {
        localStorage.setItem('token', JSON.stringify(user));
        this.isLogged = true;
        return user;
      })
    );
  }

  logout() {
    localStorage.removeItem('token');
    this.isLogged = false;
  }

  public getToken() {
    return localStorage.getItem('token');
  }

  public isAuthenticated(): boolean {
    const token = this.getToken();
    if (token) {
      const isExpired: boolean = this.jwtHelper.isTokenExpired();
      if (isExpired) { this.logout(); }
      return !isExpired;
    } else {
      return false;
    }
  }
}
