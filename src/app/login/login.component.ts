import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private fb: FormBuilder, private authService: AuthService) { }
  formGroup: FormGroup;
  validEmail = true;
  validPassword = true;

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern('((?=.*[A-Z]).{8,})')]],
    });
  }

  login() {
    this.validEmail = true;
    this.validPassword = true;
    if (this.formGroup.get('email')?.invalid) {
      this.validEmail = false;
    }
    if (this.formGroup.get('password')?.invalid) {
      this.validPassword = false;
    }
    console.log(this.formGroup.value);

    this.authService
    .login(this.formGroup.get('email')?.value, this.formGroup.get('password')?.value)
    .pipe(take(1))
    .subscribe(
      () => {
        console.log('success');
      },
      error => {
        console.log(error);
      },
    );
  }

  isLoggedIn(){
    return this.authService.isAuthenticated();
  }

  logout() {
    this.authService.logout();
    console.log(this.authService.isAuthenticated());
  }

}
